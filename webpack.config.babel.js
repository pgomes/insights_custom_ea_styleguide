var ExtractTextPlugin = require('extract-text-webpack-plugin');
var path = require('path');
var webpack = require('webpack');

/**
 * Webpack configuration.
 */

module.exports = {
  entry: {
    styleguide: `./src/styleguide.scss`
  },
  module: {
    loaders: [
      {
        include: [
          /node_modules\/font-awesome/,
          /src/
        ],
        loader: ExtractTextPlugin.extract({
          fallbackLoader: 'style-loader',
          loader: 'css-loader!sass-loader',
          options: {
            root: path.resolve(process.cwd(), 'src')
          }
        }),
        test: /\.scss$/
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('[name].min.css'),
  ],
  output: {
    filename: '[name].min.css',
    path: path.resolve(process.cwd(), 'dist')
  }
};
